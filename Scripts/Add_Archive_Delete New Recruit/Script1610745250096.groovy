import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.click(findTestObject('Top Navigation Menu/Recruits'))

WebUI.click(findTestObject('Recruits Page/ADD RECRUIT button'))

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/First Name field'), 'Josh')

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/Last Name field'), 'Allen')

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/Class field'), '2023')

WebUI.click(findTestObject('Recruits Page/Add New Recruit modal/Height field input'))

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/Height field input'), '69')

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/Birthdate field input'), '1/1/2005')

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/Birthdate field Label'))

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/High School field input'), 'highland park')

WebUI.sendKeys(findTestObject('Recruits Page/Add New Recruit modal/High School field input'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Recruits Page/Add New Recruit modal/High School field input'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/High School Coach Name field'), 'Coach Miller')

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/High School Coach Phone field'), '8474713163')

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/Club Team field'), 'Mac Irvin Fire')

WebUI.setText(findTestObject('Recruits Page/Add New Recruit modal/Club Team Number field'), '10')

WebUI.click(findTestObject('Recruits Page/Add New Recruit modal/SAVE button'))

WebUI.setText(findTestObject('Recruits Page/Recruit Search Field on Recruits page'), 'josh allen')

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruits Page/Archive recruit'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/All Recruits List selection'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/Archived List selection'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruits Page/Archived list ellipsis button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruits Page/Delete1'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruits Page/Delete2'))

WebUI.delay(2)

WebUI.closeBrowser()

