import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email2)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('Prospects View/Add Prospect button'))

WebUI.enhancedClick(findTestObject('Prospects View/Link Player Stats button'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/League_Division dropdown'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/D-II League'))

WebUI.enhancedClick(findTestObject('Prospects View/Link Player Stats Team dropdown (Prospects)'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Florida Southern (LFS)'))

WebUI.enhancedClick(findTestObject('Prospects View/Link Player Stats Player dropdown (Prospects)'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Player10 (LFS)'))

WebUI.click(findTestObject('Recruits Page/Add New Recruit modal/SAVE button'))

WebUI.mouseOver(findTestObject('Prospects View/Select Existing Prospect'))

WebUI.enhancedClick(findTestObject('Prospects View/Select Existing Prospect'))

WebUI.enhancedClick(findTestObject('Prospects View/FastScout Team Link (Prospect Profile)'))

WebUI.switchToWindowIndex(1)

WebUI.delay(7)

scoutURL = WebUI.getUrl()

assert scoutURL.contains('league=NCAAB-II')

WebUI.switchToWindowIndex(0)

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/CLOSE button'))

WebUI.delay(1)

WebUI.click(findTestObject('Prospects View/Archive Prospect button'))

WebUI.delay(1)

WebUI.click(findTestObject('Prospects View/All Prospects List selection'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/Archived List selection'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Prospects View/Archived List ellipsis (Prospects)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Prospects View/Delete1 (Prospects)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Prospects View/Delete2 (Prospects)'))

WebUI.delay(2)

WebUI.closeBrowser()

