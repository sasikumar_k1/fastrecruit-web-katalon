import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Recruits'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Dashboard'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Toggle Left (Calendar Dashboard)'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Toggle Right (Calendar Dashboard)'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Dashboard Calendar SHOW dropdown'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/My Calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/My Calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Team calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Team calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Tasks calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Tasks calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/NCAA Calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/NCAA Calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/My Live Period calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/My Live Period calendar option'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Dashboard Calendar SHOW dropdown'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Dashboard Calendar Add New'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Event Name field'))

WebUI.sendKeys(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Event Name field'), 'Test Event1')

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Event Location field'))

WebUI.sendKeys(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Event Location field'), '444 N. Michigan Ave')

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Select searched location'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Select Recruits dropdown'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Recruit1 in dropdown (Add Event modal)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Add Event Attendees'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Recruiters checkbox'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Game Note field'))

WebUI.sendKeys(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Game Note field'), 'arrive early')

WebUI.enhancedClick(findTestObject('Dashboard Page/Calendar (Dashboard)/Add Event modal/Add Event Save button'))

WebUI.delay(2)

WebUI.verifyTextPresent('Test Event1', false)

WebUI.closeBrowser()

