<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Depth Chart season dropdown</name>
   <tag></tag>
   <elementGuidId>f007d87a-ddc3-47d3-b788-3e0a32cf36ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'depthChartSeasonSelect']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>depthChartSeasonSelect</value>
   </webElementProperties>
</WebElementEntity>
