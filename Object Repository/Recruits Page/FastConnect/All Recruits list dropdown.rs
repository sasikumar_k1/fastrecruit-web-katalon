<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>All Recruits list dropdown</name>
   <tag></tag>
   <elementGuidId>339f0241-e6b3-488a-aed5-a5cf46891a73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div/section/div/div[3]/div[1]/div[1]/div[2]/div[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div/section/div/div[3]/div[1]/div[1]/div[2]/div[2]/div/div[2]</value>
   </webElementProperties>
</WebElementEntity>
