<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FastScout Team Link (Prospect Profile)</name>
   <tag></tag>
   <elementGuidId>35c11808-0ccc-45ac-9244-890bce4edb6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;recruit-info&quot;]/div/div/section/div[4]/div/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;recruit-info&quot;]/div/div/section/div[4]/div/div[2]/a</value>
   </webElementProperties>
</WebElementEntity>
